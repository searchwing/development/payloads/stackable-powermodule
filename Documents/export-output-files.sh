#!/bin/zsh

KICAD_CLI_COMMAND=kicad-cli-nightly # kicad-cli or kicad-cli-nightly
PRO_NAME=stackable-powermodule

# Enter script location directory
cd $(dirname "$0")

OUT=./output
FAB=${OUT}/Fabrication
GER=${FAB}/Gerbers

# create output dir
rm -drf ${OUT}
echo "=> Creating output directories: ${GER}"
mkdir -p ${GER} 

# Export Schematics
echo "=> Exporting schematics"
${KICAD_CLI_COMMAND} sch export pdf -o ${OUT}/SCH-${PRO_NAME}.pdf ../${PRO_NAME}.kicad_sch 

# Export layer buildup pdf
echo "=> exporting layers to multiple pdf's"
${KICAD_CLI_COMMAND} pcb export pdf --ibt -l F.Cu ../${PRO_NAME}.kicad_pcb -o "F.tmp.pdf"
${KICAD_CLI_COMMAND} pcb export pdf --ibt -l B.Cu ../${PRO_NAME}.kicad_pcb -o "B.tmp.pdf"
${KICAD_CLI_COMMAND} pcb export pdf --ibt -l In1.Cu ../${PRO_NAME}.kicad_pcb -o "1.tmp.pdf"
${KICAD_CLI_COMMAND} pcb export pdf --ibt -l In2.Cu ../${PRO_NAME}.kicad_pcb -o "2.tmp.pdf"
${KICAD_CLI_COMMAND} pcb export pdf --ibt -l F.Fab ../${PRO_NAME}.kicad_pcb -o "F.FAB.tmp.pdf"
${KICAD_CLI_COMMAND} pcb export pdf --ibt -l B.Fab ../${PRO_NAME}.kicad_pcb -o "B.FAB.tmp.pdf"
## Merge layers to one pdf
echo "=> merging layer pdf's to single pdf"
gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=${OUT}/LAYERS-${PRO_NAME}.pdf F.tmp.pdf F.FAB.tmp.pdf 1.tmp.pdf 2.tmp.pdf B.tmp.pdf B.FAB.tmp.pdf
echo "=> deleting single-layer pdf's"
rm *tmp.pdf

# Export Fabrication files
## Gerbers
echo "=> exporting gerber files"
${KICAD_CLI_COMMAND} pcb export gerbers --layers F.Cu,In1.Cu,In2.Cu,B.Cu,F.Mask,B.Mask,F.Paste,B.Paste,F.SilkS,B.SilkS,Edge.Cuts --no-x2 --output ${GER}/ ../${PRO_NAME}.kicad_pcb
## Drill
echo "=> exporting drill files"
${KICAD_CLI_COMMAND} pcb export drill --format excellon --excellon-zeros-format suppressleading --units mm --excellon-min-header --generate-map --map-format ps --output ${GER}/ ../${PRO_NAME}.kicad_pcb
## Position
echo "=> exporting position files"
${KICAD_CLI_COMMAND} pcb export pos --side front --format ascii --units mm --smd-only --exclude-fp-th --output ${GER}/${PRO_NAME}.pos ../${PRO_NAME}.kicad_pcb
## Zip Fabrication files
echo "=> zipping fabrication files"
zip -j ${FAB}/gerber-files ${GER}/*

## BOM
echo "=> exporting xml BOM"
${KICAD_CLI_COMMAND} sch export python-bom ../${PRO_NAME}.kicad_sch --output ./searchwing-py-bom/BOM-${PRO_NAME}.xml
echo "=> creating xlsx BOM"
cd searchwing-py-bom
python3 searchwing_bom.py  "BOM-${PRO_NAME}.xml" "out.csv"
echo "=> deleting temporary bom files"
rm out.csv BOM-${PRO_NAME}.xml
mv out.xlsx ../${FAB}/BOM-${PRO_NAME}.xlsx
cd ..


# stackable-powermodule

Some more documentation is found in the SearchWing [wiki](https://wiki.searchwing.org/en/home/Development/Electronics/electronics-stack).

## Generate Fabrication files

In order to export Gerber, Drill, Position and BOM files, run the script `Documents/export-output-files.sh`